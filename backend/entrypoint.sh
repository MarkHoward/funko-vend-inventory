#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z database 5432; do
    sleep 0.1
done

echo "PostgreSQL started"

alembic upgrade head
alembic revision --autogenerate
alembic upgrade head

exec "$@"
