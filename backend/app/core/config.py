import secrets

from pydantic import BaseModel, BaseSettings


class Settings(BaseSettings):

    SECRET_KEY: str = secrets.token_urlsafe(32)

    JWT_ALGORITHM: str = "HS256"
    TOKEN_LOCATION: tuple = ("headers", "cookies")
    REQUIRE_SECURE_COOKIES: bool = True
    CSRF_PROTECT_COOKIES: bool = False
    AUTHJWT_COOKIE_SAMESITE: str = "lax"

    ACCESS_TOKEN_LIFETIME: int = 900
    REFRESH_TOKEN_LIFETIME: int = 86400

    DATABASE_URL: str

    class Config:
        case_sensitive = True


settings = Settings()


class JWTSettings(BaseModel):

    authjwt_secret_key: str = settings.SECRET_KEY
    authjwt_algorithm: str = settings.JWT_ALGORITHM
    authjwt_access_token_expires: int = settings.ACCESS_TOKEN_LIFETIME
    authjwt_refresh_token_expires: int = settings.REFRESH_TOKEN_LIFETIME
    authjwt_token_location: tuple = settings.TOKEN_LOCATION
    authjwt_cookie_secure: bool = settings.REQUIRE_SECURE_COOKIES
    authjwt_cookie_csrf_protect: bool = settings.CSRF_PROTECT_COOKIES
    authjwt_cookie_samesite: str = settings.AUTHJWT_COOKIE_SAMESITE


jwt_settings = JWTSettings()
