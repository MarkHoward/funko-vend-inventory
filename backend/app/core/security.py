import re

from passlib.context import CryptContext

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)


def validate_password(password: str) -> str:
    result = ""
    if len(password) < 8:
        result += "Password must be at least 8 characters.\n"
    if password == password.upper():
        result += "Password must contain at least 1 lowercase character.\n"
    if password == password.lower():
        result += "Password must contain at least 1 uppercase character.\n"
    if re.search(r"\d", password) is None:
        result += "Password must contain at least 1 number.\n"
    if re.search(r"\W", password) is None:
        result += "Password must contain at least 1 special character.\n"
    return result
