import datetime
import os
from typing import Any

import openpyxl
from fastapi import APIRouter, Depends, File, Form, HTTPException, UploadFile
from fastapi_jwt_auth import AuthJWT
from sqlalchemy.orm import Session

from app import crud, schemas
from app.api import deps
from app.schemas import ItemCreate, OrderCreate


router = APIRouter()


def convert_field_to_int(field: str) -> int:
    try:
        return int(field)
    except ValueError:
        return 0


def convert_field_to_float(field: str) -> float:
    try:
        return float(field)
    except ValueError:
        return 0.0


def catalog_database_update(db, item, headers):
    db_item = crud.find_item_by_sku(db, sku=item[headers["sku"]])
    if db_item is not None:
        crud.update_item(db, db_obj=db_item, obj_in=item)
    else:
        item_in = ItemCreate(
            item_code=item[headers["item_code"]],
            sku=item[headers["sku"]],
            name=item[headers["name"]],
            case_quantity=convert_field_to_int(item[headers["case_quantity"]]),
            tier1_price=convert_field_to_float(item[headers["tier1"]]),
            tier2_price=convert_field_to_float(item[headers["tier2"]]),
            tier3_price=convert_field_to_float(item[headers["tier3"]]),
            tier4_price=convert_field_to_float(item[headers["tier4"]]),
        )
        crud.create_item(db, obj_in=item_in)


def report_database_update(db, order, headers):
    item = crud.find_item_by_sku(db, sku=order[headers["sku"]])
    if item is None:
        return

    order_date = None
    if headers["date"] in order.keys():
        order_date = datetime.datetime.strptime(order[headers["date"]], "%m/%d/%Y")

    order_in = OrderCreate(
        date=order_date,
        item_id=item.id,
        quantity=convert_field_to_int(order[headers["quantity"]]),
    )
    crud.create_order(db, obj_in=order_in)


@router.post("/upload", response_model=schemas.Msg)
async def upload(
    *,
    db: Session = Depends(deps.get_db),
    catalog: UploadFile = File(...),
    catalog_workSheet: str = Form(...),
    catalog_headerRow: int = Form(...),
    catalog_skuHeader: str = Form(...),
    catalog_itemCodeHeader: str = Form(...),
    catalog_nameHeader: str = Form(...),
    catalog_caseQuantityHeader: str = Form(...),
    catalog_tier1Header: str = Form(...),
    catalog_tier2Header: str = Form(...),
    catalog_tier3Header: str = Form(...),
    catalog_tier4Header: str = Form(...),
    report: UploadFile = File(...),
    report_workSheet: str = Form(...),
    report_headerRow: int = Form(...),
    report_skuHeader: str = Form(...),
    report_dateHeader: str = Form(...),
    report_quantityHeader: str = Form(...),
    Authorize: AuthJWT = Depends(),
) -> Any:
    if catalog is None and report is None:
        raise HTTPException(status_code=400, detail="Catalog and/or report required")
    if catalog is not None:
        with open("uploads/tmp_catalog.xlsx", "wb") as f:
            while content := await catalog.read(1024):
                f.write(content)
        await catalog.close()
    if report is not None:
        with open("uploads/tmp_report.xlsx", "wb") as f:
            while content := await report.read(1024):
                f.write(content)
        await report.close()

    catalog = openpyxl.load_workbook("uploads/tmp_catalog.xlsx")
    catalog = catalog.worksheets[catalog.sheetnames.index(catalog_workSheet)]
    catalog = [row for row in catalog.rows]

    report = openpyxl.load_workbook("uploads/tmp_report.xlsx")
    report = report.worksheets[report.sheetnames.index(report_workSheet)]
    report = [row for row in report.rows]

    catalog_headers = [cell.value for cell in catalog[catalog_headerRow - 1]]
    report_headers = [cell.value for cell in report[report_headerRow - 1]]

    crud.clear_orders(db)

    for item in catalog[catalog_headerRow:]:
        item = {
            header: item_cell.value for header, item_cell in zip(catalog_headers, item)
        }
        try:
            catalog_database_update(
                db,
                item,
                {
                    "sku": catalog_skuHeader,
                    "item_code": catalog_itemCodeHeader,
                    "name": catalog_nameHeader,
                    "case_quantity": catalog_caseQuantityHeader,
                    "tier1": catalog_tier1Header,
                    "tier2": catalog_tier2Header,
                    "tier3": catalog_tier3Header,
                    "tier4": catalog_tier4Header,
                },
            )
        except KeyError:
            raise HTTPException(status_code=400, detail="Error reading catalog")
    for order in report[report_headerRow:]:
        order = {
            header: order_cell.value
            for header, order_cell in zip(report_headers, order)
        }
        try:
            report_database_update(
                db,
                order,
                {
                    "sku": report_skuHeader,
                    "date": report_dateHeader,
                    "quantity": report_quantityHeader,
                },
            )
        except KeyError:
            raise HTTPException(status_code=400, detail="Error reading report")

    os.rename("uploads/tmp_catalog.xlsx", "uploads/catalog.xlsx")
    os.rename("uploads/tmp_report.xlsx", "uploads/report.xlsx")

    return {"msg": "Upload successful"}
