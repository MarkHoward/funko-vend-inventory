from typing import Any

from fastapi import APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm
from fastapi_jwt_auth import AuthJWT
from sqlalchemy.orm import Session

from app import crud, schemas
from app.api import deps

router = APIRouter()


@router.post("/login", response_model=schemas.Token)
def login(
    db: Session = Depends(deps.get_db),
    form_data: OAuth2PasswordRequestForm = Depends(),
    Authorize: AuthJWT = Depends(),
) -> Any:
    user = crud.authenticate(
        db, username=form_data.username, password=form_data.password
    )
    if user is None:
        raise HTTPException(status_code=400, detail="Incorrect username or password")

    refresh_token = Authorize.create_refresh_token(subject=user.id)
    access_token = Authorize.create_access_token(
        subject=user.id, user_claims={"id": user.id, "username": user.username}
    )

    Authorize.set_refresh_cookies(refresh_token)
    return {
        "access_token": access_token,
        "token_type": "bearer",
    }


@router.post("/login/refresh", response_model=schemas.Token)
def refresh_access_token(
    db: Session = Depends(deps.get_db),
    Authorize: AuthJWT = Depends(),
) -> Any:
    Authorize.jwt_refresh_token_required()
    current_user_id = Authorize.get_jwt_subject()
    user = crud.find_user(db, id=current_user_id)

    if user is None:
        raise HTTPException(status_code=400, detail="Could not find user for token")

    new_access_token = Authorize.create_access_token(
        subject=current_user_id, user_claims={"id": user.id, "username": user.username}
    )
    return {
        "access_token": new_access_token,
        "token_type": "bearer",
    }


@router.get("/logout", response_model=schemas.Msg)
def logout(Authorize: AuthJWT = Depends()):
    Authorize.unset_jwt_cookies()
    return {"msg": "Successfully logged out"}
