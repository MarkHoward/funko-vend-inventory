from typing import Generator, Optional

from fastapi import Depends
from fastapi_jwt_auth import AuthJWT
from sqlalchemy.orm import Session

from app import crud, models
from app.db import SessionLocal


def get_db() -> Generator:
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


def get_current_user(
    db: Session = Depends(get_db),
    Authorize: AuthJWT = Depends(),
) -> Optional[models.User]:
    current_user_id = Authorize.get_jwt_subject()
    return crud.find_user(db, id=current_user_id)
