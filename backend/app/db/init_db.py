from sqlalchemy.orm import Session

from app import crud, schemas
from app.db import base  # noqa: F401


def init_db(db: Session) -> None:
    """Create the default superuser."""
    user = crud.find_user_by_username(db, username="brad")
    if not user:
        user_in = schemas.UserCreate(username="brad", password="P@ssw0rd")
        user = crud.create_user(db, obj_in=user_in)  # noqa: F841
