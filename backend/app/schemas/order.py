from datetime import datetime
from typing import Optional, Union

from fastapi_camelcase import CamelModel


class OrderBase(CamelModel):

    date: Union[datetime, None]
    item_id: int
    quantity: int


class OrderCreate(OrderBase):
    pass


class OrderUpdate(OrderBase):
    pass


class OrderInDBBase(OrderBase):

    id: Optional[int] = None

    class Config:
        orm_mode = True


class Order(OrderInDBBase):
    pass


class OrderInDB(OrderInDBBase):
    pass
