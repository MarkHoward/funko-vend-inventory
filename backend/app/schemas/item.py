from typing import List, Optional

from fastapi_camelcase import CamelModel

from app.schemas.order import Order


class ItemBase(CamelModel):

    item_code: str
    sku: str
    name: str
    case_quantity: int
    tier1_price: float
    tier2_price: Optional[float] = 0.0
    tier3_price: Optional[float] = 0.0
    tier4_price: Optional[float] = 0.0
    on_hand: Optional[int] = 0


class ItemCreate(ItemBase):
    pass


class ItemUpdate(ItemBase):
    pass


class ItemInDBBase(ItemBase):

    id: Optional[int] = None

    class Config:
        orm_mode = True


class Item(ItemInDBBase):

    orders: List[Order]
    on_order: Optional[int] = 0


class ItemInDB(ItemInDBBase):
    pass
