from typing import Optional

from fastapi_camelcase import CamelModel


class UserBase(CamelModel):

    username: str


class UserCreate(UserBase):

    password: str


class UserUpdate(UserBase):

    password: Optional[str] = None


class UserInDBBase(UserBase):

    id: Optional[int] = None

    class Config:
        orm_mode = True


class User(UserInDBBase):
    pass


class UserInDB(UserInDBBase):

    hashed_password: str
