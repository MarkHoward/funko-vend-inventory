from typing import Optional

from fastapi_camelcase import CamelModel


class Token(CamelModel):

    access_token: str
    token_type: str


class TokenPayload(CamelModel):

    sub: Optional[int] = None
