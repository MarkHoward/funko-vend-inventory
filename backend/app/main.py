from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from fastapi_jwt_auth import AuthJWT
from fastapi_jwt_auth.exceptions import AuthJWTException

from app.api.api import api_router
from app.core.config import jwt_settings
from app.db import SessionLocal, init_db

tags = [
    {"name": "items", "description": "Item operations."},
    {"name": "login", "description": "Session operations."},
    {"name": "upload", "description": "File upload operations."},
    {"name": "users", "description": "User operations."},
]
app = FastAPI(
    title="Brad's Toys",
    description="App Description",
    version="0.0.0",
    openapi_tags=tags,
)

app.include_router(api_router, prefix="/api")


@AuthJWT.load_config
def get_config():
    """Load JWT settings."""
    return jwt_settings


@app.exception_handler(AuthJWTException)
def authjwt_exception_handler(request: Request, exc: AuthJWTException):
    """Set JWT exception handling."""
    return JSONResponse(status_code=exc.status_code, content={"detail": exc.message})


init_db(SessionLocal())
