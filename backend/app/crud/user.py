from typing import Any, Dict, List, Optional, Union

from sqlalchemy.orm import Session

from app.core.security import get_password_hash, verify_password
from app.crud.base import CRUDBase
from app.models import User
from app.schemas import UserCreate, UserUpdate


class CRUDUser(CRUDBase[User, UserCreate, UserUpdate]):
    pass


crud_user = CRUDUser(User)


def find_user(db: Session, id: Any) -> Optional[User]:
    return crud_user.find(db, id)


def list_users(db: Session, *, skip: int = 0, limit: int = 100) -> List[User]:
    return crud_user.list(db, skip=skip, limit=limit)


def find_user_by_username(db: Session, *, username: str) -> Optional[User]:
    return db.query(User).filter(User.username == username).first()


def create_user(db: Session, *, obj_in: UserCreate) -> User:
    db_obj = User(
        username=obj_in.username, hashed_password=get_password_hash(obj_in.password)
    )  # type: ignore
    db.add(db_obj)
    db.commit()
    db.refresh(db_obj)
    return db_obj


def update_user(
    db: Session, *, db_obj: User, obj_in: Union[UserUpdate, Dict[str, Any]]
) -> User:
    if isinstance(obj_in, dict):
        update_data = obj_in
    else:
        update_data = obj_in.dict(exclude_unset=True)
    if "password" in update_data.keys():
        hashed_password = get_password_hash(update_data["password"])
        del update_data["password"]
        update_data["hashed_password"] = hashed_password
    return crud_user.update(db, db_obj=db_obj, obj_in=update_data)


def authenticate(db: Session, *, username: str, password: str) -> Optional[User]:
    user = find_user_by_username(db, username=username)
    if user is not None and not verify_password(password, user.hashed_password):
        return None
    return user
