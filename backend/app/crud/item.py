from typing import Any, Dict, List, Optional, Union

from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models import Item
from app.schemas import ItemCreate, ItemUpdate


class CRUDItem(CRUDBase[Item, ItemCreate, ItemUpdate]):
    pass


crud_item = CRUDItem(Item)


def find_item(db: Session, id: Any) -> Optional[Item]:
    return crud_item.find(db, id)


def find_item_by_sku(db: Session, *, sku: int) -> Optional[Item]:
    return db.query(Item).filter(Item.sku == sku).first()


def list_items(db: Session, *, skip: int = 0, limit: int = 100) -> List[Item]:
    return crud_item.list(db, skip=skip, limit=limit)


def create_item(db: Session, *, obj_in: ItemCreate) -> Item:
    db_obj = Item(
        item_code=obj_in.item_code,
        sku=obj_in.sku,
        name=obj_in.name,
        case_quantity=obj_in.case_quantity,
        tier1_price=obj_in.tier1_price,
        tier2_price=obj_in.tier2_price,
        tier3_price=obj_in.tier3_price,
        tier4_price=obj_in.tier4_price,
        on_hand=obj_in.on_hand,
    )  # type: ignore
    db.add(db_obj)
    db.commit()
    db.refresh(db_obj)
    return db_obj


def update_item(
    db: Session, *, db_obj: Item, obj_in: Union[ItemUpdate, Dict[str, Any]]
) -> Item:
    if isinstance(obj_in, dict):
        update_data = obj_in
    else:
        update_data = obj_in.dict(exclude_unset=True)
    return crud_item.update(db, db_obj=db_obj, obj_in=update_data)
