from typing import Any, Dict, List, Optional, Union

from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models import Order
from app.schemas import OrderCreate, OrderUpdate


class CRUDOrder(CRUDBase[Order, OrderCreate, OrderUpdate]):
    pass


crud_order = CRUDOrder(Order)


def find_order(db: Session, id: Any) -> Optional[Order]:
    return crud_order.find(db, id)


def list_orders(db: Session, *, skip: int = 0, limit: int = 100) -> List[Order]:
    return crud_order.list(db, skip=skip, limit=limit)


def create_order(db: Session, *, obj_in: OrderCreate) -> Order:
    db_obj = Order(
        date=obj_in.date, item_id=obj_in.item_id, quantity=obj_in.quantity
    )  # type: ignore
    db.add(db_obj)
    db.commit()
    db.refresh(db_obj)
    return db_obj


def update_order(
    db: Session, *, db_obj: Order, obj_in: Union[OrderUpdate, Dict[str, Any]]
) -> Order:
    if isinstance(obj_in, dict):
        update_data = obj_in
    else:
        update_data = obj_in.dict(exclude_unset=True)
    return crud_order.update(db, db_obj=db_obj, obj_in=update_data)


def clear_orders(db: Session):
    db.query(Order).delete()
    db.commit()
