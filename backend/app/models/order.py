from sqlalchemy import Column, DateTime, ForeignKey, Integer

from app.db.base_class import Base


class Order(Base):

    id = Column(Integer, primary_key=True, index=True)
    item_id = Column(Integer, ForeignKey("items.id"))
    date = Column(DateTime)
    quantity = Column(Integer)
