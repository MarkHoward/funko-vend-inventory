from sqlalchemy import Column, Float, Integer, String
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Item(Base):

    id = Column(Integer, primary_key=True, index=True)
    orders = relationship("Order")
    item_code = Column(String, unique=True, nullable=False)
    sku = Column(String, unique=True, nullable=False)
    name = Column(String, nullable=False)
    on_hand = Column(Integer)
    case_quantity = Column(Integer)
    tier1_price = Column(Float)
    tier2_price = Column(Float)
    tier3_price = Column(Float)
    tier4_price = Column(Float)
