import time

import schedule

from poller.fetch import fetch

schedule.every().day.at("04:00").do(fetch)
while True:
    time.sleep(1)
    schedule.run_pending()
