from typing import Any, Dict

import requests

from poller.db import find_item_by_sku, update_item
from poller.settings import settings


def requests_with_retry(url, headers):
    response = requests.get(url, headers=headers)
    while response.status_code != 200:
        response = requests.get(url, headers=headers)
    return response


def fetch_data(endpoint):
    headers = {"Authorization": f"Bearer {settings.API_TOKEN}"}
    response = requests_with_retry(
        f"{settings.API_URL}/{endpoint}?page_size={settings.PAGE_SIZE}",
        headers=headers,
    )
    data = response.json()["data"]
    while len(response.json()["data"]) == settings.PAGE_SIZE:
        after = response.json()["version"]["max"]
        response = requests_with_retry(
            f"{settings.API_URL}/{endpoint}"
            + f"?page_size={settings.PAGE_SIZE}&after={after}",
            headers,
        )
        data += response.json()["data"]
    return data


def consolidate_inventory(inventory, products):
    inventory_amount: Dict[str, Any] = {}
    for item in inventory:
        if item["product_id"] in inventory_amount.keys():
            inventory_amount[item["product_id"]] += item["current_amount"]
        else:
            inventory_amount[item["product_id"]] = item["current_amount"]

    consolidated_inventory = {}
    for item in products:
        if item["id"] in inventory_amount.keys():
            consolidated_inventory[item["sku"]] = inventory_amount[item["id"]]
        else:
            consolidated_inventory[item["sku"]] = 0

    return consolidated_inventory


def update_database(sku, on_hand):
    item = find_item_by_sku(sku)
    if item is not None:
        update_item(item, on_hand)


def fetch():
    products = fetch_data("products")
    inventory = fetch_data("inventory")

    inventory = consolidate_inventory(inventory, products)
    for sku, qty in inventory.items():
        update_database(sku, qty)
