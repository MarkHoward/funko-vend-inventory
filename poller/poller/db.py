from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from poller.settings import settings

engine = create_engine(settings.DATABASE_URL, pool_pre_ping=True)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

db = SessionLocal()


def find_item_by_sku(sku):
    return db.execute(f"SELECT * FROM items WHERE sku = '{sku}'").first()


def update_item(item, on_hand):
    db.execute(f"UPDATE items SET on_hand = {on_hand} WHERE id = {item['id']}")
    db.commit()
