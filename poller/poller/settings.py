from pydantic import BaseSettings


class Settings(BaseSettings):

    DATABASE_URL: str
    API_URL: str
    API_TOKEN: str
    PAGE_SIZE: int

    class Config:
        case_sensitive = True


settings = Settings()
