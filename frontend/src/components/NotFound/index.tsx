import { Grid, Typography } from "@material-ui/core";
import React from "react";

const NotFound: React.FC = () => {
  return (
    <>
      <Grid item>
        <Typography>404</Typography>
      </Grid>
    </>
  );
};

export default NotFound;
