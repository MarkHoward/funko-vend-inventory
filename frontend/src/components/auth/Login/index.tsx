import {
  Button,
  CircularProgress,
  Grid,
  TextField,
  TextFieldProps,
} from "@material-ui/core";
import React, {
  ChangeEventHandler,
  KeyboardEventHandler,
  useContext,
  useState,
} from "react";
import axios from "axios";
import SessionContext from "SessionContext";
import jwt_decode from "jwt-decode";
import { AlertBarContext } from "components/AlertBar";
import { useHistory } from "react-router-dom";
import useFormReducer, { setUsername, setPassword } from "./reducer";
import { User } from "types";

const Login: React.FC = () => {
  /* CONTEXT */
  const { setUser, refresh } = useContext(SessionContext);
  const { setOpen, setAlertText, setSeverity } = useContext(AlertBarContext);

  /* ROUTING */
  const history = useHistory();

  /* FORM STATE */
  const [formState, dispatch] = useFormReducer();
  const { username, password } = formState;
  const [loading, setLoading] = useState(false);

  const handleKeyDown: KeyboardEventHandler = (e) => {
    if (e.key === "Enter") {
      handleSubmit();
    }
  };

  const handleChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    switch (e.target.name) {
      case "username":
        dispatch(setUsername(e.target.value));
        break;
      case "password":
        dispatch(setPassword(e.target.value));
        break;
    }
  };

  const handleSubmit = async () => {
    dispatch(setUsername(username.value));
    dispatch(setPassword(password.value));
    if (username.error || password.error) {
      return;
    }

    setLoading(true);

    const formData = new FormData();
    formData.append("username", username.value);
    formData.append("password", password.value);

    try {
      const response = await axios.post("/api/login", formData);
      login(response.data.accessToken);
    } catch (error) {
      setAlertText("Invalid username and/or password");
      setSeverity("error");
      setOpen(true);
    }
    setLoading(false);
  };

  const login = (accessToken: string) => {
    axios.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`;
    const user: User = jwt_decode(accessToken);

    if (history.location.pathname === "/login") {
      history.push("/");
    }

    setUser(user);
    // token is good for 900 seconds - refresh every 870 seconds, or 870000 ms
    setTimeout(refresh, 870000);
  };

  /* COMPONENT PROPS */
  const usernameFieldProps: TextFieldProps = {
    label: "Username",
    name: "username",
    id: "username",
    onChange: handleChange,
    onKeyDown: handleKeyDown,
    ...username,
  };

  const passwordFieldProps: TextFieldProps = {
    label: "Password",
    name: "password",
    type: "password",
    id: "password",
    onChange: handleChange,
    onKeyDown: handleKeyDown,
    ...password,
  };

  return (
    <>
      <Grid item>
        <TextField {...usernameFieldProps} />
      </Grid>
      <Grid item>
        <TextField {...passwordFieldProps} />
      </Grid>
      <Grid item>
        <Button variant="contained" onClick={handleSubmit} disabled={loading}>
          Submit
          {loading && <CircularProgress size={25} />}
        </Button>
      </Grid>
    </>
  );
};

export default Login;
