import { useReducer } from "react";

/* ACTIONS */
export const setUsername = (username: string) =>
  <const>{
    type: "SET_USERNAME",
    value: username,
  };

export const setPassword = (password: string) =>
  <const>{
    type: "SET_PASSWORD",
    value: password,
  };

/* INITIAL STATE */
const initialState = {
  username: { value: "", error: false, helperText: "" },
  password: { value: "", error: false, helperText: "" },
};

/* TYPING */
type State = typeof initialState;
type Action = ReturnType<typeof setUsername | typeof setPassword>;

/* VALIDATORS */
const validateUsername = (username: string) => {
  if (username === "") {
    return { error: true, helperText: "Please enter a username" };
  }
  return { error: false, helperText: "" };
};

const validatePassword = (password: string) => {
  if (password === "") {
    return { error: true, helperText: "Please enter a password" };
  }
  return { error: false, helperText: "" };
};

/* REDUCER */
const reducer = (state: State, action: Action): State => {
  let error: boolean, helperText: string;
  switch (action.type) {
    case "SET_USERNAME":
      ({ error, helperText } = validateUsername(action.value));
      return {
        ...state,
        username: { value: action.value, error, helperText },
      };
    case "SET_PASSWORD":
      ({ error, helperText } = validatePassword(action.value));
      return {
        ...state,
        password: { value: action.value, error, helperText },
      };
  }
};

export default function useLoginFormReducer(
  state = initialState
): ReturnType<typeof useReducer> {
  return useReducer(reducer, state);
}
