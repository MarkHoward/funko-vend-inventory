import React, { useState, useEffect } from "react";
import { Item } from "types";
import {
  Grid,
  TextField,
  makeStyles,
  Backdrop,
  CircularProgress,
} from "@material-ui/core";
import axios from "axios";
import { useParams } from "react-router-dom";

/* CONSTANTS */
const sidebarWidth = 200;

/* TYPES */
interface RouteParams {
  id: string;
}

/* STYLING */
const useStyles = makeStyles(() => ({
  backdrop: {
    left: sidebarWidth,
  },
}));

const ItemDetails: React.FC = () => {
  /* STYLING */
  const classes = useStyles();

  /* ROUTING */
  const params = useParams<RouteParams>();

  /* STATE */
  const [item, setItem] = useState<Item | null>(null);
  const [loading, setLoading] = useState(true);

  const fetchData = async () => {
    const response = await axios.get(`/api/${params.id}`);
    setItem(response.data);
    setLoading(false);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <Backdrop className={classes.backdrop} open={loading}>
        <CircularProgress />
      </Backdrop>
      <Grid item>
        <TextField label="SKU" id="sku" disabled value={item.sku} />
      </Grid>
      <Grid item>
        <TextField label="Item Code" id="itemCode" disabled value={item.itemCode} />
      </Grid>
      <Grid item>
        <TextField label="Name" id="name" disabled value={item.name} />
      </Grid>
      <Grid item>
        <TextField label="On Hand" id="onHand" disabled value={item.onHand} />
      </Grid>
      <Grid item>
        <TextField label="On Order" id="onOrder" disabled value={item.onOrder} />
      </Grid>
      <Grid item>
        <TextField
          label="Tier 1 Price"
          id="tier1Price"
          disabled
          value={item.tier1Price}
        />
      </Grid>
      <Grid item>
        <TextField
          label="Tier 2 Price"
          id="tier2Price"
          disabled
          value={item.tier2Price}
        />
      </Grid>
      <Grid item>
        <TextField
          label="Tier 3 Price"
          id="tier3Price"
          disabled
          value={item.tier3Price}
        />
      </Grid>
      <Grid item>
        <TextField
          label="Quantity per Case"
          id="caseQuantity"
          disabled
          value={item.caseQuantity}
        />
      </Grid>
    </>
  );
};

export default ItemDetails;
