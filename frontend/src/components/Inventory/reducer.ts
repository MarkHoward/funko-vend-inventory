import { useReducer } from "react";
import { Item } from "types";

/* ACTIONS */
export const setItems = (items: Array<Item>) =>
  <const>{
    type: "SET_ITEMS",
    value: items,
  };

export const adjustOrdering = (header: string) =>
  <const>{
    type: "ADJUST_ORDERING",
    value: header,
  };

/* INITIAL STATE */
const initialState = {
  items: [],
  direction: "asc" as Order,
  orderBy: "grade",
};

/* TYPING */
type State = typeof initialState;
type Order = "asc" | "desc";
type Action = ReturnType<typeof setItems | typeof adjustOrdering>;

/* SORT */
const sort = (users: Array<Item>, orderBy: string, direction: Order) => {
  return users.sort((a, b) => {
    let comparison = 0;
    if (a[orderBy] < b[orderBy]) {
      comparison = -1;
    } else if (a[orderBy] > b[orderBy]) {
      comparison = 1;
    }
    if (direction === "desc") {
      comparison *= -1;
    }
    return comparison;
  });
};

/* REDUCER */
const reducer = (state: State, action: Action): State => {
  let items: Array<Item>;
  switch (action.type) {
    case "SET_ITEMS":
      items = sort(action.value, state.orderBy, state.direction);
      return { ...state, items };
    case "ADJUST_ORDERING":
      if (action.value === state.orderBy) {
        const direction =
          state.direction === "asc" ? ("desc" as Order) : ("asc" as Order);
        items = sort(state.items, state.orderBy, direction);
        return { ...state, items, direction };
      }
      const direction = "asc" as Order;
      items = sort(state.items, action.value, direction);
      return { items, direction, orderBy: action.value };
  }
};

export default function useViewUsersSortReducer(
  state = initialState
): ReturnType<typeof useReducer> {
  return useReducer(reducer, state);
}
