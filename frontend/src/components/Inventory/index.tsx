import {
  CircularProgress,
  makeStyles,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
} from "@material-ui/core";
import axios from "axios";
import React, { ChangeEventHandler, useCallback, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import useSortReducer, { setItems, adjustOrdering } from "./reducer";

/* STYLING */
const useStyles = makeStyles(() => ({
  table: {
    width: "80%",
  },
  row: {
    cursor: "pointer",
  },
}));

const Inventory: React.FC = () => {
  /* STYLING */
  const classes = useStyles();

  /* ROUTING */
  const history = useHistory();

  /* TABLE STATE */
  const [loading, setLoading] = useState(false);
  const [sortState, dispatch] = useSortReducer();
  const { items, orderBy, direction } = sortState;
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(100);

  const fetchData = useCallback(async () => {
    setLoading(true);
    let skip = 0;
    const limit = 500;
    let totalItems = [];
    while (true) {
      const response = await axios.get(`/api/items?skip=${skip}&limit=${limit}`);
      for (const item of response.data) {
        for (const order of item.orders) {
          if (order.date === null || order.date > new Date()) {
            item.onOrder += order.quantity;
          }
        }
      }
      totalItems = totalItems.concat(response.data);
      dispatch(setItems(totalItems));
      skip += limit;
      if (response.data.length !== limit) {
        break;
      }
    }
    setLoading(false);
  }, [dispatch]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleHeaderClick = (header: string) => {
    dispatch(adjustOrdering(header));
  };

  const handleItemSelect = (id: number) => {
    history.push(`/items/${id}`);
  };

  const handleChangePage = (event: unknown, newPage: number) => setPage(newPage);
  const handleChangeRowsPerPage: ChangeEventHandler<HTMLInputElement> = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <>
      {loading && <CircularProgress size={50} />}
      <Table stickyHeader className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell sortDirection={orderBy === "sku" ? direction : false}>
              <TableSortLabel
                active={orderBy === "sku"}
                direction={direction}
                onClick={() => handleHeaderClick("sku")}
              >
                SKU
              </TableSortLabel>
            </TableCell>
            <TableCell sortDirection={orderBy === "name" ? direction : false}>
              <TableSortLabel
                active={orderBy === "name"}
                direction={direction}
                onClick={() => handleHeaderClick("name")}
              >
                Name
              </TableSortLabel>
            </TableCell>
            <TableCell sortDirection={orderBy === "onHand" ? direction : false}>
              <TableSortLabel
                active={orderBy === "onHand"}
                direction={direction}
                onClick={() => handleHeaderClick("onHand")}
              >
                On Hand
              </TableSortLabel>
            </TableCell>
            <TableCell sortDirection={orderBy === "onOrder" ? direction : false}>
              <TableSortLabel
                active={orderBy === "onOrder"}
                direction={direction}
                onClick={() => handleHeaderClick("onOrder")}
              >
                On Order
              </TableSortLabel>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {items
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((item) => (
              <TableRow
                key={item.id}
                className={classes.row}
                onClick={() => handleItemSelect(item.id)}
              >
                <TableCell>{item.sku}</TableCell>
                <TableCell>{item.name}</TableCell>
                <TableCell>{item.onHand}</TableCell>
                <TableCell>{item.onOrder}</TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
      <TablePagination
        component="div"
        rowsPerPageOptions={[10, 25, 50, 100]}
        count={items.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      {loading && <CircularProgress size={50} />}
    </>
  );
};

export default Inventory;
