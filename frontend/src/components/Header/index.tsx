import { AppBar, Grid, makeStyles, Toolbar, Typography } from "@material-ui/core";
import React from "react";

/* STYLING */
const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
}));

const Header: React.FC = () => {
  /* STYLING */
  const classes = useStyles();

  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar>
        <Grid container>
          <Grid item>
            <Typography variant="h6">{"Brad's Toys - Funko"}</Typography>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
