import React from "react";
import {
  createStyles,
  FormControl,
  InputLabel,
  makeStyles,
  MenuItem,
  Select,
  SelectProps,
  Theme,
} from "@material-ui/core";

/* TYPING */
interface HeaderSelectProps {
  selectProps: SelectProps;
  headerRowContents: string[];
  label: string;
}

/* STYLING */
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 200,
    },
  })
);

const HeaderSelect: React.FC<HeaderSelectProps> = (props) => {
  /* STYLING */
  const classes = useStyles();
  /* PROPS */
  const { selectProps, headerRowContents, label } = props;

  return (
    <FormControl className={classes.formControl}>
      <InputLabel htmlFor={label}>{label}</InputLabel>
      <Select {...selectProps}>
        {headerRowContents.map((header) => (
          <MenuItem key={header} value={header}>
            {header}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default HeaderSelect;
