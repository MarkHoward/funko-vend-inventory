import { useReducer } from "react";
import { WorkBook, WorkSheet } from "xlsx";

/* ACTIONS */
export const setCatalog = (file: File, workBook: WorkBook) =>
  <const>{
    type: "SET_CATALOG",
    value1: file,
    value2: workBook,
  };

export const setReport = (file: File, workBook: WorkBook) =>
  <const>{
    type: "SET_REPORT",
    value1: file,
    value2: workBook,
  };

export const setCatalogWorkSheet = (workSheet: string) =>
  <const>{
    type: "SET_CATALOG_WORK_SHEET",
    value: workSheet,
  };

export const setReportWorkSheet = (workSheet: string) =>
  <const>{
    type: "SET_REPORT_WORK_SHEET",
    value: workSheet,
  };

export const setCatalogRowDisplay = (count: number) =>
  <const>{
    type: "SET_CATALOG_ROW_DISPLAY",
    value: count,
  };

export const setReportRowDisplay = (count: number) =>
  <const>{
    type: "SET_REPORT_ROW_DISPLAY",
    value: count,
  };

export const setCatalogColumnDisplay = (count: number) =>
  <const>{
    type: "SET_CATALOG_COLUMN_DISPLAY",
    value: count,
  };

export const setReportColumnDisplay = (count: number) =>
  <const>{
    type: "SET_REPORT_COLUMN_DISPLAY",
    value: count,
  };

export const setCatalogHeaderRow = (row: number) =>
  <const>{
    type: "SET_CATALOG_HEADER_ROW",
    value: row,
  };

export const setReportHeaderRow = (row: number) =>
  <const>{
    type: "SET_REPORT_HEADER_ROW",
    value: row,
  };

export const setCatalogSkuHeader = (header: string) =>
  <const>{
    type: "SET_CATALOG_SKU_HEADER",
    value: header,
  };

export const setCatalogItemCodeHeader = (header: string) =>
  <const>{
    type: "SET_CATALOG_ITEM_CODE_HEADER",
    value: header,
  };

export const setCatalogNameHeader = (header: string) =>
  <const>{
    type: "SET_CATALOG_NAME_HEADER",
    value: header,
  };

export const setCatalogCaseQuantityHeader = (header: string) =>
  <const>{
    type: "SET_CATALOG_CASE_QUANTITY_HEADER",
    value: header,
  };

export const setCatalogTier1Header = (header: string) =>
  <const>{
    type: "SET_CATALOG_TIER1_HEADER",
    value: header,
  };

export const setCatalogTier2Header = (header: string) =>
  <const>{
    type: "SET_CATALOG_TIER2_HEADER",
    value: header,
  };

export const setCatalogTier3Header = (header: string) =>
  <const>{
    type: "SET_CATALOG_TIER3_HEADER",
    value: header,
  };

export const setCatalogTier4Header = (header: string) =>
  <const>{
    type: "SET_CATALOG_TIER4_HEADER",
    value: header,
  };

export const setReportSkuHeader = (header: string) =>
  <const>{
    type: "SET_REPORT_SKU_HEADER",
    value: header,
  };

export const setReportDateHeader = (header: string) =>
  <const>{
    type: "SET_REPORT_DATE_HEADER",
    value: header,
  };

export const setReportQuantityHeader = (header: string) =>
  <const>{
    type: "SET_REPORT_QUANTITY_HEADER",
    value: header,
  };

/* INITIAL STATE */
const initialState = {
  catalog: {
    file: { value: null, error: false, helperText: "" },
    workBook: null,
    workSheet: { value: "Sheet1", error: false },
    headerRow: { value: 1, contents: [], error: false },
    skuHeader: { value: "UPC", error: false },
    itemCodeHeader: { value: "Item", error: false },
    nameHeader: { value: "Description", error: false },
    caseQuantityHeader: { value: "Sum of Master Case Qty", error: false },
    tier1Header: { value: "Sum of Tier 1 <1 MSTR", error: false },
    tier2Header: { value: "Sum of Tier 2 1 to <5 MSTR", error: false },
    tier3Header: { value: "Sum of Tier 3 5 to <10 MSTR", error: false },
    tier4Header: { value: "Sum of Tier 4 10+ MSTR", error: false },
    displayColumns: { value: 20 },
    displayRows: { value: 50 },
  },
  report: {
    file: { value: null, error: false, helperText: "" },
    workBook: null,
    workSheet: { value: "COMBINED", error: false },
    headerRow: { value: 18, contents: [], error: false },
    skuHeader: { value: "UPC", error: false },
    dateHeader: { value: "NEXT PLANNED SHIPMENT DATE", error: false },
    quantityHeader: { value: "TTL QTY. ALLOCATED TO LINE", error: false },
    displayColumns: { value: 60 },
    displayRows: { value: 100 },
  },
};

/* TYPING */
type State = typeof initialState;
type Action = ReturnType<
  | typeof setCatalog
  | typeof setReport
  | typeof setCatalogWorkSheet
  | typeof setReportWorkSheet
  | typeof setCatalogHeaderRow
  | typeof setReportHeaderRow
  | typeof setCatalogRowDisplay
  | typeof setReportRowDisplay
  | typeof setCatalogColumnDisplay
  | typeof setReportColumnDisplay
  | typeof setCatalogSkuHeader
  | typeof setCatalogNameHeader
  | typeof setCatalogTier1Header
  | typeof setCatalogTier2Header
  | typeof setCatalogTier3Header
  | typeof setCatalogTier4Header
  | typeof setCatalogItemCodeHeader
  | typeof setCatalogCaseQuantityHeader
  | typeof setReportSkuHeader
  | typeof setReportDateHeader
  | typeof setReportQuantityHeader
>;

/* VALIDATORS */
const validateCatalog = (file: File, workBook: WorkBook, state: State): State => {
  let error: boolean, helperText: string;

  // eslint-disable-next-line prefer-const
  ({ error, helperText } = validateFile(file));
  const catalogFile = { value: file, error, helperText };

  error = validateWorkSheet(workBook, state.catalog.workSheet.value);
  const workSheet = { value: state.catalog.workSheet.value, error };

  error = validateHeaderRow(state.catalog.headerRow.value);
  let contents = [];
  if (!error) {
    contents = getHeaderRowContents(
      state.catalog.headerRow.value,
      workBook.Sheets[workSheet.value],
      state.catalog.displayColumns.value
    );
  }
  const headerRow = { ...state.catalog.headerRow, contents, error };

  error = validateHeader(contents, state.catalog.skuHeader.value);
  const skuHeader = { ...state.catalog.skuHeader, error };
  error = validateHeader(contents, state.catalog.nameHeader.value);
  const nameHeader = { ...state.catalog.nameHeader, error };
  error = validateHeader(contents, state.catalog.tier1Header.value);
  const tier1Header = { ...state.catalog.tier1Header, error };
  error = validateHeader(contents, state.catalog.tier2Header.value);
  const tier2Header = { ...state.catalog.tier2Header, error };
  error = validateHeader(contents, state.catalog.tier3Header.value);
  const tier3Header = { ...state.catalog.tier3Header, error };
  error = validateHeader(contents, state.catalog.tier4Header.value);
  const tier4Header = { ...state.catalog.tier4Header, error };
  error = validateHeader(contents, state.catalog.itemCodeHeader.value);
  const itemCodeHeader = { ...state.catalog.itemCodeHeader, error };
  error = validateHeader(contents, state.catalog.caseQuantityHeader.value);
  const caseQuantityHeader = { ...state.catalog.caseQuantityHeader, error };

  return {
    ...state,
    catalog: {
      ...state.catalog,
      file: catalogFile,
      workBook,
      workSheet,
      headerRow,
      skuHeader,
      nameHeader,
      tier1Header,
      tier2Header,
      tier3Header,
      tier4Header,
      itemCodeHeader,
      caseQuantityHeader,
    },
  };
};

const validateReport = (file: File, workBook: WorkBook, state: State): State => {
  let error: boolean, helperText: string;

  // eslint-disable-next-line prefer-const
  ({ error, helperText } = validateFile(file));
  const reportFile = { value: file, error, helperText };

  error = validateWorkSheet(workBook, state.report.workSheet.value);
  const workSheet = { value: state.report.workSheet.value, error };

  error = validateHeaderRow(state.report.headerRow.value);
  let contents = [];
  if (!error) {
    contents = getHeaderRowContents(
      state.report.headerRow.value,
      workBook.Sheets[workSheet.value],
      state.report.displayColumns.value
    );
  }
  const headerRow = { ...state.report.headerRow, contents, error };

  error = validateHeader(contents, state.report.skuHeader.value);
  const skuHeader = { ...state.report.skuHeader, error };
  error = validateHeader(contents, state.report.dateHeader.value);
  const dateHeader = { ...state.report.dateHeader, error };
  error = validateHeader(contents, state.report.quantityHeader.value);
  const quantityHeader = { ...state.report.quantityHeader, error };

  return {
    ...state,
    report: {
      ...state.report,
      file: reportFile,
      workBook,
      workSheet,
      headerRow,
      skuHeader,
      dateHeader,
      quantityHeader,
    },
  };
};

const validateFile = (file: File) => {
  return { error: false, helperText: "" };
};

const validateWorkSheet = (workBook: WorkBook, sheetName: string) => {
  return !workBook.SheetNames.includes(sheetName);
};

const validateHeaderRow = (row: number) => {
  return false;
};

const validateHeader = (row: string[], header: string) => {
  return !row.includes(header);
};

/* REDUCER */
const reducer = (state: State, action: Action): State => {
  let error: boolean;
  switch (action.type) {
    case "SET_CATALOG":
      return validateCatalog(action.value1, action.value2, state);
    case "SET_REPORT":
      return validateReport(action.value1, action.value2, state);
    case "SET_CATALOG_WORK_SHEET":
      error = validateWorkSheet(state.catalog.workBook.value, action.value);
      return {
        ...state,
        catalog: { ...state.catalog, workSheet: { value: action.value, error } },
      };
    case "SET_REPORT_WORK_SHEET":
      error = validateWorkSheet(state.report.workBook.value, action.value);
      return {
        ...state,
        report: { ...state.report, workSheet: { value: action.value, error } },
      };
    case "SET_CATALOG_HEADER_ROW":
      error = validateHeaderRow(action.value);
      return {
        ...state,
        catalog: {
          ...state.catalog,
          headerRow: {
            value: action.value,
            contents: getHeaderRowContents(
              action.value,
              state.catalog.workBook.Sheets[state.catalog.workSheet.value],
              state.catalog.displayColumns.value
            ),
            error,
          },
        },
      };
    case "SET_REPORT_HEADER_ROW":
      error = validateHeaderRow(action.value);
      return {
        ...state,
        report: {
          ...state.report,
          headerRow: {
            value: action.value,
            contents: getHeaderRowContents(
              action.value,
              state.catalog.workBook.Sheets[state.catalog.workSheet.value],
              state.catalog.displayColumns.value
            ),
            error,
          },
        },
      };
    case "SET_CATALOG_ROW_DISPLAY":
      return {
        ...state,
        catalog: { ...state.catalog, displayRows: { value: action.value } },
      };
    case "SET_REPORT_ROW_DISPLAY":
      return {
        ...state,
        report: { ...state.report, displayRows: { value: action.value } },
      };
    case "SET_CATALOG_COLUMN_DISPLAY":
      return {
        ...state,
        catalog: { ...state.catalog, displayColumns: { value: action.value } },
      };
    case "SET_REPORT_COLUMN_DISPLAY":
      return {
        ...state,
        report: { ...state.report, displayColumns: { value: action.value } },
      };
    case "SET_CATALOG_SKU_HEADER":
      error = validateHeader(state.catalog.headerRow.contents, action.value);
      return {
        ...state,
        catalog: {
          ...state.catalog,
          skuHeader: { value: action.value, error },
        },
      };
    case "SET_CATALOG_NAME_HEADER":
      error = validateHeader(state.catalog.headerRow.contents, action.value);
      return {
        ...state,
        catalog: {
          ...state.catalog,
          nameHeader: { value: action.value, error },
        },
      };
    case "SET_CATALOG_TIER1_HEADER":
      error = validateHeader(state.catalog.headerRow.contents, action.value);
      return {
        ...state,
        catalog: {
          ...state.catalog,
          tier1Header: { value: action.value, error },
        },
      };
    case "SET_CATALOG_TIER2_HEADER":
      error = validateHeader(state.catalog.headerRow.contents, action.value);
      return {
        ...state,
        catalog: {
          ...state.catalog,
          tier2Header: { value: action.value, error },
        },
      };
    case "SET_CATALOG_TIER3_HEADER":
      error = validateHeader(state.catalog.headerRow.contents, action.value);
      return {
        ...state,
        catalog: {
          ...state.catalog,
          tier3Header: { value: action.value, error },
        },
      };
    case "SET_CATALOG_TIER4_HEADER":
      error = validateHeader(state.catalog.headerRow.contents, action.value);
      return {
        ...state,
        catalog: {
          ...state.catalog,
          tier4Header: { value: action.value, error },
        },
      };
    case "SET_CATALOG_ITEM_CODE_HEADER":
      error = validateHeader(state.catalog.headerRow.contents, action.value);
      return {
        ...state,
        catalog: {
          ...state.catalog,
          itemCodeHeader: { value: action.value, error },
        },
      };
    case "SET_CATALOG_CASE_QUANTITY_HEADER":
      error = validateHeader(state.catalog.headerRow.contents, action.value);
      return {
        ...state,
        catalog: {
          ...state.catalog,
          caseQuantityHeader: { value: action.value, error },
        },
      };
    case "SET_REPORT_SKU_HEADER":
      error = validateHeader(state.catalog.headerRow.contents, action.value);
      return {
        ...state,
        report: {
          ...state.report,
          skuHeader: { value: action.value, error },
        },
      };
    case "SET_REPORT_DATE_HEADER":
      error = validateHeader(state.catalog.headerRow.contents, action.value);
      return {
        ...state,
        report: {
          ...state.report,
          dateHeader: { value: action.value, error },
        },
      };
    case "SET_REPORT_QUANTITY_HEADER":
      error = validateHeader(state.catalog.headerRow.contents, action.value);
      return {
        ...state,
        report: {
          ...state.report,
          quantityHeader: { value: action.value, error },
        },
      };
  }
};

const getHeaderRowContents = (
  rowIndex: number,
  workSheet: WorkSheet,
  maxLength: number
) => {
  const row = [];
  const alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  for (let i = 0; i < maxLength; i++) {
    const loops = Math.floor(i / 26);
    let cellAddress = "";
    if (loops === 0) {
      cellAddress = `${alph[i]}${rowIndex}`;
    } else {
      cellAddress = `${alph[loops - 1]}${alph[i % 26]}${rowIndex}`;
    }
    if (workSheet[cellAddress] && workSheet[cellAddress].v) {
      row.push(workSheet[cellAddress].v);
    }
  }
  return row;
};

export default function useTableFormReducer(
  state = initialState
): ReturnType<typeof useReducer> {
  return useReducer(reducer, state);
}
