import React, { ChangeEventHandler, useContext, useState } from "react";
import {
  Grid,
  Input,
  Button,
  InputLabel,
  Paper,
  makeStyles,
  Select,
  MenuItem,
  SelectProps,
  TextFieldProps,
  TextField,
  FormControl,
  Typography,
  Theme,
  CircularProgress,
  InputProps,
} from "@material-ui/core";
import { AlertBarContext } from "components/AlertBar";
import axios from "axios";
import ExcelViewer from "components/ExcelViewer";
import xlsx from "xlsx";
import useFormReducer, {
  setCatalog,
  setReport,
  setCatalogWorkSheet,
  setReportWorkSheet,
  setCatalogHeaderRow,
  setReportHeaderRow,
  setCatalogRowDisplay,
  setReportRowDisplay,
  setCatalogColumnDisplay,
  setReportColumnDisplay,
  setCatalogSkuHeader,
  setCatalogNameHeader,
  setCatalogTier1Header,
  setCatalogTier2Header,
  setCatalogTier3Header,
  setCatalogTier4Header,
  setCatalogItemCodeHeader,
  setCatalogCaseQuantityHeader,
  setReportSkuHeader,
  setReportDateHeader,
  setReportQuantityHeader,
} from "./reducer";
import HeaderSelect from "./HeaderSelect";

/* STYLING */
const useStyles = makeStyles((theme: Theme) => ({
  paper: {
    width: "1200px",
    height: "400px",
    overflow: "scroll",
    margin: theme.spacing(1),
  },
  headerRowInput: {
    margin: theme.spacing(1),
    minWidth: 200,
  },
}));

const Upload: React.FC = () => {
  /* STYLING */
  const classes = useStyles();

  /* CONTEXT */
  const { setOpen, setAlertText, setSeverity } = useContext(AlertBarContext);

  /* FORM STATE */
  const [loading, setLoading] = useState(false);
  const [formState, dispatch] = useFormReducer();

  const handleFileSelect: ChangeEventHandler<HTMLInputElement> = (event) => {
    setLoading(true);
    const reader = new FileReader();
    reader.onload = (e) => {
      const buffer = e.target.result as Buffer;
      const data = new Uint8Array(buffer);
      const workBook = xlsx.read(data, { type: "array" });
      switch (event.target.name) {
        case "catalog":
          dispatch(setCatalog(event.target.files[0], workBook));
          break;
        case "report":
          dispatch(setReport(event.target.files[0], workBook));
          break;
      }
      setLoading(false);
    };
    reader.readAsArrayBuffer(event.target.files[0]);
  };

  const handleChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    switch (e.target.name) {
      case "catalogWorkSheet":
        dispatch(setCatalogWorkSheet(e.target.value));
        break;
      case "reportWorkSheet":
        dispatch(setReportWorkSheet(e.target.value));
        break;
      case "catalogHeaderRow":
        dispatch(setCatalogHeaderRow(Number(e.target.value)));
        break;
      case "reportHeaderRow":
        dispatch(setReportHeaderRow(Number(e.target.value)));
        break;
      case "catalogRowDisplay":
        dispatch(setCatalogRowDisplay(Number(e.target.value)));
        break;
      case "reportRowDisplay":
        dispatch(setReportRowDisplay(Number(e.target.value)));
        break;
      case "catalogColumnDisplay":
        dispatch(setCatalogColumnDisplay(Number(e.target.value)));
        break;
      case "reportColumnDisplay":
        dispatch(setReportColumnDisplay(Number(e.target.value)));
        break;
      case "catalogSkuHeader":
        dispatch(setCatalogSkuHeader(e.target.value));
        break;
      case "catalogNameHeader":
        dispatch(setCatalogNameHeader(e.target.value));
        break;
      case "catalogTier1Header":
        dispatch(setCatalogTier1Header(e.target.value));
        break;
      case "catalogTier2Header":
        dispatch(setCatalogTier2Header(e.target.value));
        break;
      case "catalogTier3Header":
        dispatch(setCatalogTier3Header(e.target.value));
        break;
      case "catalogTier4Header":
        dispatch(setCatalogTier4Header(e.target.value));
        break;
      case "catalogItemCodeHeader":
        dispatch(setCatalogItemCodeHeader(e.target.value));
        break;
      case "catalogCaseQuantityHeader":
        dispatch(setCatalogCaseQuantityHeader(e.target.value));
        break;
      case "reportSkuHeader":
        dispatch(setReportSkuHeader(e.target.value));
        break;
      case "reportDateHeader":
        dispatch(setReportDateHeader(e.target.value));
        break;
      case "reportQuantityHeader":
        dispatch(setReportQuantityHeader(e.target.value));
        break;
    }
  };

  const handleSubmit = async () => {
    if (
      Object.keys(formState.catalog).filter((key) => formState.catalog[key].error)
        .length > 0 ||
      Object.keys(formState.report).filter((key) => formState.report[key].error)
        .length > 0
    ) {
      return;
    }
    setLoading(true);
    if (formState.catalog.file.value === null && formState.report.file.value === null) {
      setAlertText("Must select at least one file to upload");
      setSeverity("error");
    } else {
      const formData = new FormData();
      if (formState.catalog.file.value) {
        formData.append(
          "catalog",
          formState.catalog.file.value,
          formState.catalog.file.value.name
        );
        Object.entries(formState.catalog).forEach((entry) => {
          if (entry[1].hasOwnProperty("value")) {
            formData.append(`catalog_${entry[0]}`, entry[1]["value"]);
          }
        });
      }
      if (formState.report.file.value) {
        formData.append(
          "report",
          formState.report.file.value,
          formState.report.file.value.name
        );
        Object.entries(formState.report).forEach((entry) => {
          if (entry[1].hasOwnProperty("value")) {
            formData.append(`report_${entry[0]}`, entry[1]["value"]);
          }
        });
      }
      try {
        await axios.post("/api/upload", formData);
        setAlertText("Upload successful");
        setSeverity("success");
      } catch (error) {
        setAlertText(error.response.data.detail);
        setSeverity("error");
      }
    }
    setLoading(false);
    setOpen(true);
  };

  /* COMPONENT PROPS */
  const baseProps: InputProps = {
    onChange: handleChange,
    disabled: loading,
  };

  const catalogWorkSheetProps: SelectProps = {
    name: "catalogWorkSheet",
    ...baseProps,
    ...formState.catalog.workSheet,
  };

  const reportWorkSheetProps: SelectProps = {
    name: "reportWorkSheet",
    ...baseProps,
    ...formState.report.workSheet,
  };

  const catalogHeaderRowProps: TextFieldProps = {
    type: "number",
    name: "catalogHeaderRow",
    label: "Header Row",
    className: classes.headerRowInput,
    ...baseProps,
    ...formState.catalog.headerRow,
  };

  const reportHeaderRowProps: TextFieldProps = {
    type: "number",
    name: "reportHeaderRow",
    label: "Header Row",
    className: classes.headerRowInput,
    ...baseProps,
    ...formState.report.headerRow,
  };

  const catalogRowDisplayProps: TextFieldProps = {
    type: "number",
    name: "catalogRowDisplay",
    label: "Row Preview",
    ...baseProps,
    ...formState.catalog.displayRows,
  };

  const reportRowDisplayProps: TextFieldProps = {
    type: "number",
    name: "reportRowDisplay",
    label: "Row Preview",
    ...baseProps,
    ...formState.report.displayRows,
  };

  const catalogColumnDisplayProps: TextFieldProps = {
    type: "number",
    name: "catalogColumnDisplay",
    label: "Column Preview",
    ...baseProps,
    ...formState.catalog.displayColumns,
  };

  const reportColumnDisplayProps: TextFieldProps = {
    type: "number",
    name: "reportColumnDisplay",
    label: "Column Preview",
    ...baseProps,
    ...formState.report.displayColumns,
  };

  const catalogSkuHeaderProps: SelectProps = {
    name: "catalogSkuHeader",
    ...baseProps,
    ...formState.catalog.skuHeader,
  };

  const catalogNameHeaderProps: SelectProps = {
    name: "catalogNameHeader",
    ...baseProps,
    ...formState.catalog.nameHeader,
  };

  const catalogTier1HeaderProps: SelectProps = {
    name: "catalogTier1Header",
    ...baseProps,
    ...formState.catalog.tier1Header,
  };

  const catalogTier2HeaderProps: SelectProps = {
    name: "catalogTier2Header",
    ...baseProps,
    ...formState.catalog.tier2Header,
  };

  const catalogTier3HeaderProps: SelectProps = {
    name: "catalogTier3Header",
    ...baseProps,
    ...formState.catalog.tier3Header,
  };

  const catalogTier4HeaderProps: SelectProps = {
    name: "catalogTier4Header",
    ...baseProps,
    ...formState.catalog.tier4Header,
  };

  const catalogItemCodeHeaderProps: SelectProps = {
    name: "catalogItemCodeHeader",
    ...baseProps,
    ...formState.catalog.itemCodeHeader,
  };

  const catalogCaseQuantityHeaderProps: SelectProps = {
    name: "catalogCaseQuantityHeader",
    ...baseProps,
    ...formState.catalog.caseQuantityHeader,
  };

  const reportSkuHeaderProps: SelectProps = {
    name: "reportSkuHeader",
    ...baseProps,
    ...formState.report.skuHeader,
  };

  const reportDateHeaderProps: SelectProps = {
    name: "reportDateHeader",
    ...baseProps,
    ...formState.report.dateHeader,
  };

  const reportQuantityHeaderProps: SelectProps = {
    name: "reportQuantityHeader",
    ...baseProps,
    ...formState.report.quantityHeader,
  };

  const catalogFileInputProps: InputProps = {
    type: "file",
    id: "catalog",
    name: "catalog",
    onChange: handleFileSelect,
    disabled: loading,
  };

  const reportFileInputProps: InputProps = {
    type: "file",
    id: "report",
    name: "report",
    onChange: handleFileSelect,
    disabled: loading,
  };

  return (
    <>
      <Grid item>
        <InputLabel>Funko Catalog</InputLabel>
        <Input {...catalogFileInputProps} />
      </Grid>
      <Grid item>
        <InputLabel>Order Report</InputLabel>
        <Input {...reportFileInputProps} />
      </Grid>
      <Grid item>
        <Button variant="contained" onClick={handleSubmit} disabled={loading}>
          Submit
          {loading && <CircularProgress size={25} />}
        </Button>
      </Grid>
      {formState.catalog.workBook ? (
        <>
          <Typography variant="h4">Funko Catalog</Typography>
          <Grid container justify="space-around">
            <Grid>
              <FormControl>
                <InputLabel htmlFor="catalog-worksheet">WorkSheet</InputLabel>
                <Select {...catalogWorkSheetProps}>
                  {formState.catalog.workBook.SheetNames.map((sheetName: string) => (
                    <MenuItem key={sheetName} value={sheetName}>
                      {sheetName}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item>
              <TextField {...catalogColumnDisplayProps} />
            </Grid>
            <Grid item>
              <TextField {...catalogRowDisplayProps} />
            </Grid>
          </Grid>
          <Paper className={classes.paper} variant="outlined" elevation={3}>
            <ExcelViewer
              workSheet={
                formState.catalog.workBook.Sheets[formState.catalog.workSheet.value]
              }
              properties={formState.catalog}
            />
          </Paper>
          <Grid container justify="space-around" spacing={1}>
            <Grid item xs={3}>
              <TextField {...catalogHeaderRowProps} />
            </Grid>
            <Grid item xs={4}>
              <HeaderSelect
                selectProps={catalogSkuHeaderProps}
                headerRowContents={formState.catalog.headerRow.contents}
                label="Sku"
              />
            </Grid>
            <Grid item xs={3}>
              <HeaderSelect
                selectProps={catalogNameHeaderProps}
                headerRowContents={formState.catalog.headerRow.contents}
                label="Name"
              />
            </Grid>
            <Grid item xs={3}>
              <HeaderSelect
                selectProps={catalogTier1HeaderProps}
                headerRowContents={formState.catalog.headerRow.contents}
                label="Tier 1"
              />
            </Grid>
            <Grid item xs={4}>
              <HeaderSelect
                selectProps={catalogTier2HeaderProps}
                headerRowContents={formState.catalog.headerRow.contents}
                label="Tier 2"
              />
            </Grid>
            <Grid item xs={3}>
              <HeaderSelect
                selectProps={catalogTier3HeaderProps}
                headerRowContents={formState.catalog.headerRow.contents}
                label="Tier 3"
              />
            </Grid>
            <Grid item xs={3}>
              <HeaderSelect
                selectProps={catalogTier4HeaderProps}
                headerRowContents={formState.catalog.headerRow.contents}
                label="Tier 4"
              />
            </Grid>
            <Grid item xs={4}>
              <HeaderSelect
                selectProps={catalogItemCodeHeaderProps}
                headerRowContents={formState.catalog.headerRow.contents}
                label="Item Code"
              />
            </Grid>
            <Grid item xs={3}>
              <HeaderSelect
                selectProps={catalogCaseQuantityHeaderProps}
                headerRowContents={formState.catalog.headerRow.contents}
                label="Case Quantity"
              />
            </Grid>
          </Grid>
        </>
      ) : null}
      {formState.report.workBook ? (
        <>
          <Typography variant="h4">Order Report</Typography>
          <Grid container justify="space-around">
            <Grid item>
              <FormControl>
                <InputLabel htmlFor="report-worksheet">WorkSheet</InputLabel>
                <Select {...reportWorkSheetProps}>
                  {formState.report.workBook.SheetNames.map((sheetName: string) => (
                    <MenuItem key={sheetName} value={sheetName}>
                      {sheetName}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item>
              <TextField {...reportColumnDisplayProps} />
            </Grid>
            <Grid item>
              <TextField {...reportRowDisplayProps} />
            </Grid>
          </Grid>
          <Paper className={classes.paper} variant="outlined" elevation={3}>
            <ExcelViewer
              workSheet={
                formState.report.workBook.Sheets[formState.report.workSheet.value]
              }
              properties={formState.report}
            />
          </Paper>
          <Grid container justify="space-around">
            <Grid item>
              <TextField {...reportHeaderRowProps} />
            </Grid>
            <Grid item>
              <HeaderSelect
                selectProps={reportSkuHeaderProps}
                headerRowContents={formState.report.headerRow.contents}
                label="Sku"
              />
            </Grid>
            <Grid item>
              <HeaderSelect
                selectProps={reportDateHeaderProps}
                headerRowContents={formState.report.headerRow.contents}
                label="Date"
              />
            </Grid>
            <Grid item>
              <HeaderSelect
                selectProps={reportQuantityHeaderProps}
                headerRowContents={formState.report.headerRow.contents}
                label="Quantity"
              />
            </Grid>
          </Grid>
        </>
      ) : null}
    </>
  );
};

export default Upload;
