import {
  Button,
  CircularProgress,
  Grid,
  TextField,
  TextFieldProps,
} from "@material-ui/core";
import axios from "axios";
import React, {
  ChangeEventHandler,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { AlertBarContext } from "components/AlertBar";
import useFormReducer, { setUsername, setPassword1, setPassword2 } from "./reducer";
import SessionContext from "SessionContext";

const ViewProfile: React.FC = () => {
  /* CONTEXT */
  const { setOpen, setAlertText, setSeverity } = useContext(AlertBarContext);
  const { user } = useContext(SessionContext);

  /* FORM STATE */
  const [formState, dispatch] = useFormReducer();
  const { username, password1, password2 } = formState;
  const [loading, setLoading] = useState(false);

  const fetchData = useCallback(async () => {
    setLoading(true);
    const response = await axios.get(`/api/users/${user.id}`);
    dispatch(setUsername(response.data.username));
    setLoading(false);
  }, [dispatch]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleChange: ChangeEventHandler<HTMLInputElement> = (e) => {
    switch (e.target.name) {
      case "password1":
        dispatch(setPassword1(e.target.value));
        break;
      case "password2":
        dispatch(setPassword2(e.target.value));
        break;
    }
  };

  const handleSubmit = async () => {
    if (password1.value === "" || password2.value === "") {
      dispatch(setPassword1(password1.value));
      dispatch(setPassword2(password2.value));
      return;
    }
    if (password1.error || password2.error) {
      return;
    }
    setLoading(true);
    try {
      await axios.put(`/api/users/${user.id}`, {
        username: username.value,
        password: password1.value,
      });
      setAlertText("Successfully updated password");
      setSeverity("success");
    } catch (error) {
      setAlertText(error.response.data.detail);
      setSeverity("error");
    }
    setOpen(true);
    setLoading(false);
  };

  /* COMPONENT PROPERTIES */
  const usernameFieldProps: TextFieldProps = {
    label: "Username",
    id: "username",
    disabled: true,
    ...username,
  };

  const password1FieldProps: TextFieldProps = {
    label: "New Password",
    type: "password",
    name: "password1",
    id: "password1",
    onChange: handleChange,
    disabled: loading,
    ...password1,
  };

  const password2FieldProps: TextFieldProps = {
    label: "Confirm Password",
    type: "password",
    name: "password2",
    id: "password2",
    onChange: handleChange,
    disabled: loading,
    ...password2,
  };

  return (
    <>
      <Grid item>
        <TextField {...usernameFieldProps} />
      </Grid>
      <Grid item>
        <TextField {...password1FieldProps} />
      </Grid>
      <Grid item>
        <TextField {...password2FieldProps} />
      </Grid>
      <Grid item>
        <Button variant="contained" onClick={handleSubmit} disabled={loading}>
          Change Password
          {loading && <CircularProgress size={25} />}
        </Button>
      </Grid>
    </>
  );
};

export default ViewProfile;
