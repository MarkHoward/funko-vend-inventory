import { useReducer } from "react";
import checkPasswordComplexity from "helpers/checkPasswordComplexity";

/* ACTIONS */
export const setUsername = (username: string) =>
  <const>{
    type: "SET_USERNAME",
    value: username,
  };

export const setPassword1 = (password: string) =>
  <const>{
    type: "SET_PASSWORD1",
    value: password,
  };

export const setPassword2 = (password: string) =>
  <const>{
    type: "SET_PASSWORD2",
    value: password,
  };

/* INITIAL STATE */
const initialState = {
  username: { value: "" },
  password1: { value: "", error: false, helperText: "" },
  password2: { value: "", error: false, helperText: "" },
};

/* TYPING */
type State = typeof initialState;
type Action = ReturnType<
  typeof setUsername | typeof setPassword1 | typeof setPassword2
>;

/* VALIDATORS */
const validatePassword = (password: string, otherPassword: string) => {
  if (password === "") {
    return { error: true, helperText: "Please enter a password" };
  }
  if (otherPassword !== "" && password !== otherPassword) {
    return { error: true, helperText: "Passwords do not match" };
  }

  const validationErrors = checkPasswordComplexity(password);

  return {
    error: !!validationErrors,
    helperText: validationErrors,
  };
};

/* REDUCER */
const reducer = (state: State, action: Action): State => {
  let error: boolean, helperText: string;
  switch (action.type) {
    case "SET_USERNAME":
      return {
        ...state,
        username: { value: action.value },
      };
    case "SET_PASSWORD1":
      ({ error, helperText } = validatePassword(action.value, state.password2.value));
      return {
        ...state,
        password1: { value: action.value, error, helperText },
      };
    case "SET_PASSWORD2":
      ({ error, helperText } = validatePassword(action.value, state.password1.value));
      return {
        ...state,
        password2: { value: action.value, error, helperText },
      };
  }
};

export default function useProfileFormReducer(
  state = initialState
): ReturnType<typeof useReducer> {
  return useReducer(reducer, state);
}
