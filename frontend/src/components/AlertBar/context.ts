import { Color } from "@material-ui/lab/Alert";
import { createContext, Dispatch, SetStateAction } from "react";

interface AlertBarContext {
  setOpen: Dispatch<SetStateAction<boolean>>;
  setAlertText: Dispatch<SetStateAction<string>>;
  setSeverity: Dispatch<SetStateAction<Color>>;
}

export const context = createContext<AlertBarContext | null>(null);
