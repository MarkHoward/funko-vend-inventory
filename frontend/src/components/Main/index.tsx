import { Grid, makeStyles, Toolbar } from "@material-ui/core";
import { Switch, Route } from "react-router-dom";
import React, { useContext } from "react";
import SessionContext from "SessionContext";
import Login from "components/auth/Login";
import Logout from "components/auth/Logout";
import ViewProfile from "components/ViewProfile";
import NotFound from "components/NotFound";
import Home from "components/Home";
import Inventory from "components/Inventory";
import ItemDetails from "components/ItemDetails";
import Upload from "components/Upload";

/* STYLING */
const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    passing: theme.spacing(3),
  },
}));

const Main: React.FC = () => {
  /* STYLING */
  const classes = useStyles();

  /* CONTEXT */
  const { user } = useContext(SessionContext);

  /* ROUTING */
  const routes = [
    <Route exact path="/logout" key="/logout">
      <Logout />
    </Route>,
  ];
  if (user.id) {
    routes.push(
      <Route exact path="/upload" key="/upload">
        <Upload />
      </Route>
    );
    routes.push(
      <Route exact path="/inventory" key="/inventory">
        <Inventory />
      </Route>
    );
    routes.push(
      <Route exact path="/items/:id" key="/items/:id">
        <ItemDetails />
      </Route>
    );
    routes.push(
      <Route exact path="/profile" key="/profile">
        <ViewProfile />
      </Route>
    );
    routes.push(
      <Route exact path="/" key="/">
        <Home />
      </Route>
    );
    routes.push(
      <Route path="/" key="/~">
        <NotFound />
      </Route>
    );
  } else {
    routes.push(
      <Route path="/" key="/~">
        <Login />
      </Route>
    );
  }
  return (
    <main className={classes.content}>
      <Grid
        container
        direction="column"
        justify="space-around"
        alignItems="center"
        spacing={3}
      >
        <Grid item>
          <Toolbar />
        </Grid>
        <Switch>{routes}</Switch>
      </Grid>
    </main>
  );
};

export default Main;
