import { Table, TableBody, TableCell, TableHead, TableRow } from "@material-ui/core";
import React, { useState } from "react";
import { useEffect } from "react";
import { WorkSheet } from "xlsx";

/* TYPES */
interface CatalogProperties {
  workSheet: { value: string };
  headerRow: { value: number };
  skuHeader: { value: string };
  itemCodeHeader: { value: string };
  nameHeader: { value: string };
  caseQuantityHeader: { value: string };
  tier1Header: { value: string };
  tier2Header: { value: string };
  tier3Header: { value: string };
  tier4Header: { value: string };
  displayColumns: { value: number };
  displayRows: { value: number };
}
interface ReportProperties {
  workSheet: { value: string };
  headerRow: { value: number };
  skuHeader: { value: string };
  dateHeader: { value: string };
  quantityHeader: { value: string };
  displayColumns: { value: number };
  displayRows: { value: number };
}
interface ExcelViewerProps {
  workSheet: WorkSheet;
  properties: CatalogProperties | ReportProperties;
}

const ExcelViewer: React.FC<ExcelViewerProps> = (props) => {
  /* PROPS */
  const { workSheet, properties } = props;

  /* TABLE STATE */
  const [tableData, setTableData] = useState<string[][]>([[]]);

  useEffect(() => {
    const table = [buildHeaderRow()];
    for (let i = 0; i < properties.displayRows.value; i++) {
      const row = [`${i + 1}`];
      for (let j = 0; j < properties.displayColumns.value; j++) {
        row.push(getCellAddress(i, j));
      }
      table.push(row);
    }
    setTableData(table);
  }, []);

  const buildHeaderRow = () => {
    const alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const row = [""];
    for (let i = 0; i < properties.displayColumns.value; i++) {
      const loops = Math.floor(i / 26);
      if (loops === 0) {
        row.push(`${alph[i]}`);
      } else {
        row.push(`${alph[loops - 1]}${alph[i % 26]}`);
      }
    }
    return row;
  };

  const getCellAddress = (row: number, column: number): string => {
    const alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const loops = Math.floor(column / 26);
    column = column % 26;
    if (loops === 0) {
      return `${alph[column]}${row + 1}`;
    }
    return `${alph[loops - 1]}${alph[column]}${row + 1}`;
  };

  return (
    <>
      <Table stickyHeader>
        <TableHead>
          <TableRow>
            {tableData[0].map((cell) => (
              <TableCell key={cell}>{cell}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {tableData.slice(1).map((row) => (
            <TableRow key={row[0]}>
              <TableCell>{row[0]}</TableCell>
              {row.slice(1).map((cell) => (
                <TableCell key={cell}>
                  {workSheet[cell] ? workSheet[cell].v : ""}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </>
  );
};

export default ExcelViewer;
