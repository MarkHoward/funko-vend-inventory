import {
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
  Toolbar,
} from "@material-ui/core";
import React, { forwardRef, useMemo, useContext, Ref, HTMLAttributes } from "react";
import { Link as RouterLink } from "react-router-dom";
import SessionContext from "SessionContext";

/* CONSTANTS */
const drawerWidth = 200;

/* TYPING */
interface ListItemLinkProps {
  icon?: string;
  primary: string;
  to: string;
}

/* OTHER COMPONENTS */
const ListItemLink = (props: ListItemLinkProps) => {
  const { icon, primary, to } = props;

  const renderLink = useMemo(() => {
    const routerLink = (
      itemProps: HTMLAttributes<HTMLAnchorElement>,
      ref: Ref<HTMLAnchorElement>
    ) => <RouterLink to={to} ref={ref} {...itemProps} />;
    return forwardRef(routerLink);
  }, [to]);

  return (
    <li>
      <ListItem button component={renderLink}>
        {icon && <ListItemIcon>{icon}</ListItemIcon>}
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  );
};

/* STYLING */
const useStyles = makeStyles(() => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: "auto",
  },
}));

const Sidebar: React.FC = () => {
  /* STYLING */
  const classes = useStyles();

  /* CONTEXT */
  const { user } = useContext(SessionContext);

  /* ROUTING */
  const links = [];
  if (user.id) {
    links.push(<ListItemLink to="/profile" primary="Profile" key="profile" />);
    links.push(<ListItemLink to="/logout" primary="Logout" key="logout" />);
    links.push(<Divider key="auth-divider" />);
    links.push(<ListItemLink to="/inventory" primary="Inventory" key="inventory" />);
    links.push(<ListItemLink to="/upload" primary="Upload" key="upload" />);
  } else {
    links.push(<ListItemLink to="/login" primary="Login" key="login" />);
  }

  return (
    <Drawer
      variant="permanent"
      className={classes.drawer}
      classes={{ paper: classes.drawerPaper }}
    >
      <Toolbar />
      <div className={classes.drawerContainer}>
        <List>{links}</List>
      </div>
    </Drawer>
  );
};

export default Sidebar;
