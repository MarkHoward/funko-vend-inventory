import React, { useCallback, useEffect, useMemo, useState } from "react";
import { ThemeProvider } from "@material-ui/core/styles";
import Theme from "Theme";
import Header from "components/Header";
import Sidebar from "components/Sidebar";
import Main from "components/Main";
import SessionContext from "SessionContext";
import { CssBaseline, makeStyles } from "@material-ui/core";
import { BrowserRouter as Router } from "react-router-dom";
import axios from "axios";
import jwt_decode from "jwt-decode";
import AlertBar, { AlertBarContext } from "components/AlertBar";
import { Color } from "@material-ui/lab/Alert";
import { User } from "types";

/* STYLING */
const useStyles = makeStyles(() => ({
  app: {
    display: "flex",
  },
}));

const App: React.FC = () => {
  /* STYLING */
  const classes = useStyles();

  /* SESSION CONTEXT */
  const noUser: User = useMemo(() => {
    return {
      id: 0,
      username: "",
    };
  }, []);
  const [user, setUser] = useState<User>(noUser);

  const syncLogout = useCallback(() => {
    delete axios.defaults.headers.common["Authorization"];
    setUser(noUser);
  }, [noUser]);

  const login = useCallback((accessToken) => {
    axios.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`;
    const decoded: User = jwt_decode(accessToken);
    setUser(decoded);
  }, []);

  const refresh = useCallback(async () => {
    try {
      const response = await axios.post(
        "/api/login/refresh",
        {},
        { headers: { Authorization: "" } }
      );
      login(response.data.accessToken);
      // token is good for 900 seconds - refresh every 870 seconds, or 870000 ms
      setTimeout(refresh, 870000);
    } catch (error) {
      syncLogout();
    }
  }, [login, syncLogout]);

  /* ALERTBAR CONTEXT */
  const [open, setOpen] = useState<boolean>(false);
  const [alertText, setAlertText] = useState<string>("");
  const [severity, setSeverity] = useState<Color>("error");

  /* AUTO-LOGIN ON PAGE LOAD */
  useEffect(() => {
    refresh();
  }, [refresh]);

  /* SYNC SESSION STATE ACROSS TABS */
  window.onstorage = () => {
    refresh();
  };

  return (
    <SessionContext.Provider value={{ user, setUser, refresh, syncLogout }}>
      <AlertBarContext.Provider value={{ setOpen, setAlertText, setSeverity }}>
        <ThemeProvider theme={Theme}>
          <div className={classes.app}>
            <Router>
              <CssBaseline />
              <Header />
              <Sidebar />
              <Main />
              <AlertBar
                open={open}
                onClose={() => setOpen(false)}
                alertText={alertText}
                severity={severity}
              />
            </Router>
          </div>
        </ThemeProvider>
      </AlertBarContext.Provider>
    </SessionContext.Provider>
  );
};

export default App;
