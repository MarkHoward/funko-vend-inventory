import { createContext, Dispatch, SetStateAction } from "react";
import { User } from "./types";

interface SessionContext {
  user: User;
  setUser: Dispatch<SetStateAction<User>>;
  syncLogout: () => void;
  refresh: () => void;
}

export default createContext<SessionContext | null>(null);
