export type User = {
  id: number;
  username: string;
};

export type Order = {
  id: number;
  date: Date;
  quantity: number;
  itemId: number;
};

export type Item = {
  id: number;
  onOrder: number;
  onHand: number;
  itemCode: string;
  sku: string;
  name: string;
  caseQuantity: number;
  tier1Price: number;
  tier2Price: number;
  tier3Price: number;
  tier4Price: number;
};
