/* eslint-disable */
import path from "path";
import webpack from "webpack";
import HtmlWebpackPlugin from "html-webpack-plugin";
import ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";
import { TsconfigPathsPlugin } from "tsconfig-paths-webpack-plugin";

let devServer = {};
if (process.env.CONTAINER) {
  devServer = {
    sockPort: 443,
    publicPath: "/",
    historyApiFallback: true,
    https: true,
    watchOptions: {
      poll: true,
    },
  };
}

module.exports = (env) => {
  return {
    devServer,
    entry: {
      index: "./src/index.tsx",
    },
    mode: "development",
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          loader: "ts-loader",
          options: {
            transpileOnly: true,
          },
          exclude: /dist/,
        },
      ],
    },
    output: {
      filename: "build.js",
      path: path.join(__dirname, "/dist"),
      clean: true,
    },
    plugins: [
      new HtmlWebpackPlugin({ template: "./public/index.html" }),
      new webpack.DefinePlugin({
        "process.env.PRODUCTION": env.production || !env.development,
        "process.env.NAME": JSON.stringify(require("./package.json").name),
        "process.env.VERSION": JSON.stringify(require("./package.json").version),
      }),
      new ForkTsCheckerWebpackPlugin({
        eslint: { files: "./src/**/*.{ts,tsx,js,jsx}" },
      }),
    ],
    resolve: {
      extensions: [".ts", ".tsx", ".js"],
      plugins: [new TsconfigPathsPlugin()],
    },
  };
};
